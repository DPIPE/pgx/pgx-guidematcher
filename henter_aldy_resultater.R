hent_aldy_resultater <- function(filbane, aktuelle_gener){
  filer_aldy_result <- tibble(File_name  = list.files(filbane)) %>% #lager en liste over hvilke resultatefiler man har 
    slice(grep(pattern = "aldy",x = File_name)) %>% #velger ut bare .aldy filene 
    mutate(prve_ID = str_remove_all(str_extract(File_name,"-[[:alpha:]].[[:digit:]]*[[:alpha:]][[:digit:]][[:punct:]]"),"[[:punct:]]")) #legger inn prøve id

  aldy_resultater <- tibble()
  for (resultat in filer_aldy_result$File_name) { #forløkke som går gjennom filbaner fra listen over aldy resultater som blir laget i starten av scriptet. 
    pr_id_resultat <- read.delim(paste0(filbane,"/",resultat),sep = "\t") %>%
      filter(Gene %in% aktuelle_gener) %>% #henter ut de rader som tilhører det som er definert som aktuelle gener 
      select(c("Gene","Major")) %>% #velger ut de to viktigste kolonne 
      mutate(prve_ID = filter(filer_aldy_result,File_name == resultat)$prve_ID) %>% #henter ut prøvenummer
      distinct() %>% #fjerner rader som er like
      select(c("prve_ID","Gene","Major")) #changes the order of the columns 
    
    aldy_resultater <- rbind(aldy_resultater,pr_id_resultat) #slår sammen alle resultater til en lang liste
  }
  return(aldy_resultater) #returns the table 
}



hent_swisslab_arbeidslister <- function(filbane){
  filer_swisslab <- tibble(filnavn = list.files(filbane)) %>%
    slice(grep("swisslab",filnavn))

  #går igjennom arbeidslistene og lager en felles liste med alle 
  swisslab_rekvisisjon <- tibble()
  for (sw_al in filer_swisslab$filnavn) {
    swisslab_rekvisisjon_pr_fil <- readr::read_delim(paste0(filbane_swisslabb,"/",sw_al),col_names = T,show_col_types = FALSE) # show_col_type = FALSE fjerner tekst fra konsollen 
    
    #colnames(swisslab_rekvisisjon) <- gsub("[^[:alnum:]///' ]", "",paste0(swisslab_rekvisisjon[1,]),) #endrer overskrifter
    
    swisslab_rekvisisjon_pr_fil <- rename(swisslab_rekvisisjon_pr_fil,prve_ID = `Prve ID`)
    #swisslab_rekvisisjon <- slice(swisslab_rekvisisjon,which(swisslab_rekvisisjon$Nr != "Nr.")) %>% #fjerner raden med overskrifter
    # rename(prve_ID = `Prve ID`)
    
    swisslab_rekvisisjon <- rbind(swisslab_rekvisisjon,swisslab_rekvisisjon_pr_fil)
  }
  
  
  return(swisslab_rekvisisjon) #returns the table 
}

utvidet_aldy_res <- function(tabell){ #define the function name and parameters 
  enkel_utvidet <- tibble() #makes empty dataframe to be filled with expanded genes-allele for all samples 
  for (prove in unique(tabell$prve_ID)){ #for-loop to iterate through results 
    utvalg_prve <- filter(tabell,prve_ID == prove) #choose the results of a single sample 
    for (gen in utvalg_prve$Gene){ #for-loop to iterate through genes 
      utvalg_gen <- filter(utvalg_prve,Gene == gen) #get the results for single gene 
      genotyper <- unlist(strsplit(utvalg_gen$Major,split = "\\+|\\/")) #split the results for a single gene an create a table with one row for each allele
      enkel_utvidet <- rbind(enkel_utvidet,tibble(Gene = gen, Major = genotyper,prve_ID = prove)) #add the rows to the final table with alle the samples 
    }
  }
  enkel_utvidet <- select(enkel_utvidet,c("prve_ID","Gene","Major")) #changes the order of the columns 
  return(enkel_utvidet) #returns the table 
}
